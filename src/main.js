// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import axios from 'axios'
import store from './store/index'
//导入全局样式表
import './assets/css/global.css'
//进度条
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
//树形下拉菜单
import ElSelectTree from 'el-select-tree'
Vue.component(ElSelectTree)

//引入编辑器
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme

Vue.use(VueQuillEditor)
import { Quill } from 'vue-quill-editor'
import ImageResize from 'quill-image-resize-module'
Quill.register('modules/imageResize', ImageResize)


//引入babel-polyfill
import 'babel-polyfill'

//树形表格
import ElTreeGrid from 'element-tree-grid'

Vue.component(ElTreeGrid.name, ElTreeGrid)

//全屏组件
import Screenfull from 'screenfull'

Vue.prototype.$screenfull = Screenfull
//全局分页变量
Vue.prototype.$global_limit = 10
Vue.prototype.$global_page = 1
//axios请求
// axios.defaults.baseURL = 'http://amr_website.test/api/'
// Vue.prototype.$action_url = 'http://amr_website.test/api/dashboard/upload_place_img'
// Vue.prototype.$http_url = 'http://amr_website.test/'

axios.defaults.baseURL = 'http://www.ahamr.cn/api/'
Vue.prototype.$action_url = 'http://www.ahamr.cn/api/dashboard/upload_place_img'
Vue.prototype.$http_url = 'http://www.ahamr.cn/'

axios.interceptors.request.use(config => {
  NProgress.start()
  config.headers.Authorization = window.sessionStorage.getItem('token')
  return config
})
axios.interceptors.response.use(config => {
  NProgress.done()
  return config
  // if (config.data.code !== 200) {
  //   window.sessionStorage.clear();
  //   var vm = new Vue()
  //   vm.$message.error("Token失效,请重新登录")
  //   setTimeout(function () {
  //     return router.push('/login');
  //   }, 1000);
  // } else {
  //   return config;
  // }
})

Vue.use(ElementUI)
Vue.prototype.$http = axios
Vue.config.productionTip = false

//验证
Vue.directive('has', {
  inserted: function (el, binding) {
    if (!Vue.prototype.$is_has(binding.value)) {
      el.parentNode.removeChild(el)
    }
  }
})

Vue.prototype.$is_has = function (value) {
  let isExist = false
  let role = store.state.userInfo.url
  let role_arr = role.split(',')
  if (role_arr.includes(value)) {
    isExist = true
  }
  return isExist
}
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<App/>'
})
