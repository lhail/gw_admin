import Vue from 'vue'
import Router from 'vue-router'
import login from '../views/login'
import home from '../views/home'
import dashboard from '../views/dashboard'
import user from '../views/user'
import role from '../views/role'
import menu from '../views/menu'
import navigation from '../views/navigation'
import article from '../views/article'
import article_add from '../views/article/add'
import banner from '../views/banner'
import modular from '../views/modular'
import modular_add from '../views/modular/add'
import cooperation from '../views/cooperation'
import about from '../views/about'
import service from '../views/service'
import recruit from '../views/recruit'
import system from '../views/system'

Vue.use(Router)

const router = new Router({
  routes: [
    {path: '/', redirect: '/login'},
    {path: '/login', component: login},
    {
      path: '/home', component: home, name: 'Home', meta: {title: 'Home'},
      redirect: '/dashboard/index',
      children: [
        {path: '/dashboard/index', component: dashboard, name: '主控台', meta: {title: '主控台', type: 'menu'}},
      ]
    },
    {
      path: '/home', component: home, name: '用户管理', meta: {title: '用户管理'},
      redirect: '/user',
      children: [
        {path: '/dashboard/user_list', component: user, name: '用户列表', meta: {title: '用户列表', type: 'menu'}},
      ]
    },
    {
      path: '/home', component: home, name: '权限管理', meta: {title: '权限管理'},
      redirect: '/role',
      children: [
        {path: '/dashboard/role_list', component: role, name: '权限列表', meta: {title: '权限列表', type: 'menu'}},
        {path: '/dashboard/menu_list', component: menu, name: '菜单列表', meta: {title: '菜单列表', type: 'menu'}},
      ]
    },
    {
      path: '/home', component: home, name: '网站设置', meta: {title: '网站设置'},
      redirect: '/navigation',
      children: [
        {path: '/dashboard/nav_list', component: navigation, name: '导航栏', meta: {title: '导航列表', type: 'menu'}},
        {path: '/dashboard/banner_list', component: banner, name: '走马灯', meta: {title: '走马灯', type: 'menu'}},
        {path: '/dashboard/modular_list', component: modular, name: '首页模块', meta: {title: '首页模块', type: 'menu'}},
        {path: '/dashboard/modular/add', component: modular_add, name: '新增模块', meta: {title: '新增模块', type: 'method'}},
        {path: '/dashboard/cooperation_list', component: cooperation, name: '合作客户', meta: {title: '合作客户', type: 'menu'}},
        {path: '/dashboard/about_list', component: about, name: '关于我们', meta: {title: '关于我们', type: 'menu'}},
        {path: '/dashboard/server_list', component: service, name: '服务项目', meta: {title: '服务项目', type: 'menu'}},
        {path: '/dashboard/recruit_list', component: recruit, name: '招聘设置', meta: {title: '招聘设置', type: 'menu'}},
        {path: '/dashboard/system', component: system, name: '系统设置', meta: {title: '系统设置', type: 'menu'}},
      ]
    },
    {
      path: '/home', component: home, name: '新闻管理', meta: {title: '新闻管理'},
      redirect: '/article',
      children: [
        {path: '/dashboard/article_list', component: article, name: '新闻列表', meta: {title: '新闻列表', type: 'menu'}},
        {path: '/dashboard/article/add', component: article_add, name: '新增新闻', meta: {title: '新增新闻',type:'method'}},
      ]
    },
  ]
})

import store from '../store/index'  //引入store
//挂载路由导航守卫
router.beforeEach((to, from, next) => {
  if (to.path === '/login') return next()
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) {
    return next('./login')
  } else {
    //判断是否有访问该路径的权限
    const urls = store.state.userInfo.url

    if (to.meta.title) {//判断是否有标题
      document.title = to.meta.title
    }

    if (to.meta.type !== 'method') {
      sessionStorage.setItem('activePath', to.path)
    }
    // //console.log(store.state.userInfo.role)
    // //如果是管理员
    // if (store.state.userInfo.role === '*') {
    //   if (to.meta.type !== 'method') {
    //     sessionStorage.setItem('activePath', to.path)
    //   }
    //   return next()
    // } else {
    //   if (urls.indexOf(to.path) > -1) {
    //     //则包含该元素
    //
    //     return next()
    //   } else {
    //     return next('/401')
    //   }
    // }
  }
  next()
})
export default router

